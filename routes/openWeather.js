var openWeather = require('../controllers/openWeather.js');

module.exports = function(app, express) {

    var openWeatherRoute = express.Router();

    openWeatherRoute.route('/weathers')
        .get(openWeather.getEntireData)
        .post(openWeather.openWeather);

    return openWeatherRoute;
};