var request = require("request");
var admin = require("firebase-admin");
var moment = require("moment");
var config = require("../config/config");

admin.initializeApp({
  credential: admin.credential.cert("./config/firebase-service.json"),
  databaseURL: config.database
});

var db = admin.database();


exports.openWeather = function (req, res) {

  console.log(req.body);
  var country = req.body.country_code;
  var city = req.body.city;
  var getData = db.ref("weathers");

  // Validate input data

  if(!city || !country){
    console.log('wrong data format, please check');
    res.json('wrong data format, please check');
  }

  else {

    getData.once("value", function (snapshot) {
      var date = moment().format('YYYY-MM-DD');

      var exist = snapshot.child(country).exists() &&
          snapshot.child(country + '/' + city).exists() &&
          snapshot.child(country + '/' + city + '/' + date).exists();

      if (!exist) {
        console.log('data not exist');
        requesting(city, country, date);
      }
      else {
        console.log('data exist, checking last data update time');
        lastUpdate = snapshot.child(country + '/' + city + '/' + date).val().dt;
        console.log('current timeStamp ' + moment.utc() / 1000);
        console.log('last updated timeStamp: ' + lastUpdate);
        var diff = moment.utc() / 1000 - lastUpdate;

        if ((diff / 60) > 60) {
          console.log('differnce: ' + diff / 60 + ', more than an hour');
          requesting(city, country, date);

        }
        else {
          console.log('differnce: ' + diff / 60 + ', less than an hour');
          res.json(snapshot.child(country + '/' + city + '/' + date).val());
        }
      }
    });
  }

  var requesting = function (city,country,date) {
    console.log('in requesting');
    request({
      uri: "http://api.openweathermap.org/data/2.5/weather?APPID=3afa0c66e7782ea4a3e1d9f663af6a32&q=" + city + ',' + country + "&units=metric",
      method: "GET",
      timeout: 10000,
      followRedirect: true,
      maxRedirects: 10
    }, function (error, response, body) {
      console.log(error);
      weatherData = JSON.parse(body);
      var openWeatherMapRef = db.ref("weathers/" + country + '/' + city + '/' + date);
      openWeatherMapRef.update(weatherData);
      res.json(weatherData)
    });
  };
};

exports.getEntireData = function (req, res) {
  var openWeatherMapRef = db.ref("weathers");
  openWeatherMapRef.once("value", function (snapshot){
    res.json(snapshot.val());
  });
};
