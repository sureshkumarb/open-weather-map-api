module.exports = {
    'port': process.env.PORT || 8080,
    'database': 'https://open-weather-map-9d760.firebaseio.com/'
};