var express = require('express');
var app = express();
var bodyParser = require('body-parser'); // get body-parser
var morgan = require('morgan'); // used to see requests
var path = require('path');
var swagger = require("swagger-node-express"); // to write api doc
var config = require('./config/config')

var port    =  config.port;

// APP CONFIGURATION --------------
// use body parser so we can grab information from POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure our app to handle CORS requests
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
    next();
});

// log all request to the console
app.use(morgan('dev'));

//API DOC Url
var subpath = express();
app.use("/api-docs", subpath);
swagger.setAppHandler(subpath);

// Swagger Configuration
swagger.setApiInfo({
    description: "API to do something, manage something...",
    termsOfServiceUrl: "",
    license: "",
    licenseUrl: ""
});

// API ROUTES ------------------------
var openWeatherRoute = require('./routes/openWeather')(app, express);

app.use('/',[openWeatherRoute]);

// set the public folder to server public assets
app.use(express.static(__dirname + '/public'));

subpath.get('/', function (req, res) {
    res.sendfile(__dirname + '/public/dist/index.html');
});

swagger.configureSwaggerPaths('', 'api-docs', '');

swagger.configure("http://localhost", '1.0.0');

//  And start the app on that interface (and port).
app.listen(port);
console.log("Server Running at " + port);